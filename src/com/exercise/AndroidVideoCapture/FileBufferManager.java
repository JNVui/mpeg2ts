package com.exercise.AndroidVideoCapture;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import android.util.Log;

public class FileBufferManager extends Thread
{
  public final static String TAG ="FileBufferManager";
  //TO-DO use configuration code
  private final String PATH = "/sdcard/coyote/video/";

  public interface FileBufferUser{
    void onRandomAccessFileChanged(RandomAccessFile pRandomAccessFile);
  }

  private FileBufferUser mFileBufferUser;
  private boolean mRunning = false;
  private final int INTERVAL_BETWEEN_FILE_SWITCH = 10000;
  private final int NB_FILE = 7;


  private String [] mWritingFiles;
  private int mCurrentFileInd;
  private File mFileOpened;
  private RandomAccessFile mRandomAccessFile;
  private RandomAccessFile mRandomAccessFileHeader;

  public FileBufferManager (FileBufferUser pBufferUser){
    mFileBufferUser = pBufferUser;
    mWritingFiles = new String[NB_FILE];  

    for (int i = 0 ; i < NB_FILE ; i++ ){
      mWritingFiles[i] = PATH + i;
    }

    mCurrentFileInd = -1;
   
    try
    {
      mRandomAccessFileHeader = openFile(PATH + "header");
    }
    catch ( IOException e )
    {
      Log.e(TAG, "constructor", e);
    }
  }

  public void run(){
    mRunning = true;
    while(mRunning){
      try
      {

        mCurrentFileInd = (mCurrentFileInd + 1) % NB_FILE; 
        mRandomAccessFile = openFile(mWritingFiles[mCurrentFileInd]);
        if(mFileBufferUser != null && mRandomAccessFile != null){
          mFileBufferUser.onRandomAccessFileChanged(mRandomAccessFile);
        }

        Thread.sleep(INTERVAL_BETWEEN_FILE_SWITCH);
      }
      catch ( InterruptedException e )
      {
        Log.e(TAG, "run", e);
      }
      catch ( FileNotFoundException e )
      {
        Log.e(TAG, "run", e);
      }
      catch ( IOException e )
      {
        Log.e(TAG, "run", e);
      }
    }
  }


  public void interrupt(){
    mRunning = false;
    super.interrupt();
  }

  private RandomAccessFile openFile(String pPath) throws IOException{
    Log.d(TAG, "openingFile : " + pPath);
    
    //delete file
    mFileOpened = new File(pPath);
    mFileOpened.delete();
    
    //recreate file
    mFileOpened = new File(pPath); 
    return new RandomAccessFile(mFileOpened, "rw");

  }

  /*private void closeFile() throws IOException{
    if(mRandomAccessFile != null){
      Log.d(TAG, "closingFile");
      mRandomAccessFile.close();
    }
  }*/
  
  public boolean writeInHeader(byte[] pHeaderByte) {
    if(pHeaderByte != null && pHeaderByte.length > 0 && mRandomAccessFileHeader !=null){
      try
      {
        mRandomAccessFileHeader.write(pHeaderByte);
        mRandomAccessFileHeader.close();
        return true;
      }
      catch ( IOException e )
      {
        Log.e(TAG, "writeInHeader", e);
      }
      
    }
    return false;
  } 
}
