package com.exercise.AndroidVideoCapture;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Bundle;
import android.util.Log;
import android.util.TimingLogger;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

public class AndroidVideoCapture extends Activity implements SurfaceHolder.Callback{
  public final static String TAG = "AndroidVideoCapture";
  Button myButton;
  MediaRecorder mediaRecorder, mediaRecorderD;
  SurfaceHolder surfaceHolder;
  boolean recording, finish;
  //FileOutputStream outputStream;
  FileOutputStream outputStream;
  FileDescriptor helo;
  SocketListener mSocketListener;
  FileBufferManager mFileBufferManager;

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    recording = false;
    finish = false;

    mSocketListener = new SocketListener();
    mFileBufferManager = new FileBufferManager(mSocketListener);
    mSocketListener.setFileBufferManager(mFileBufferManager);
    mSocketListener.start();
    
    

    mediaRecorder = new MediaRecorder();
    mediaRecorderD = new MediaRecorder();
    initMediaRecorder(mediaRecorder, "/sdcard/myVideo.mp4");
    //initMediaRecorder(mediaRecorderD, "/sdcard/myVideoD.mp4");

    setContentView(R.layout.main);

    SurfaceView myVideoView = (SurfaceView)findViewById(R.id.videoview);
    surfaceHolder = myVideoView.getHolder();
    surfaceHolder.addCallback(this);
    surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    myButton = (Button)findViewById(R.id.mybutton);
    myButton.setOnClickListener(myButtonOnClickListener);


  }

  TimingLogger timings = new TimingLogger("Jips", "Video ::");


  private Button.OnClickListener myButtonOnClickListener 
  = new Button.OnClickListener(){

    @Override
    public void onClick(View arg0) {
      // TODO Auto-generated method stub
      //			if(recording){
      //				
      //				if(!finish){
      //					
      //					timings.addSplit("START");
      //					timings.addSplit("avant stop");
      //					mediaRecorder.stop();
      //					timings.addSplit("apr�s stop");
      //					
      //					//mediaRecorder.release();
      //					timings.addSplit("avant start");
      //					mediaRecorderD.start();
      //					timings.addSplit("apr�s start");
      //
      //				}
      //				//mediaRecorderD.stop();
      //				//mediaRecorderD.release();
      //				if (finish){
      //					mediaRecorderD.stop();
      //					//mediaRecorder.stop();
      //					mediaRecorderD.release();
      //					mediaRecorder.release();					
      //					
      //					finish();
      //				}
      //				finish = true;
      //			}else{
      //				mediaRecorder.start();
      //				//mediaRecorderD.start();
      //				recording = true;
      //				myButton.setText("STOP");
      //			}
      if(!recording){
        recording = true;
        mFileBufferManager.start();
        mediaRecorder.start();
        
      }
      else{
        recording = false;
        mFileBufferManager.interrupt();
        mediaRecorder.stop();    
      }
    }};


    @Override
    public void onStop() {
      super.onStop();
      timings.addSplit("JIPSJIPSJSPJSPISJPI");
      timings.dumpToLog();
      Log.d("JIPPPPPS", "WATAAAAAAAAA");
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
      // TODO Auto-generated method stub

    }
    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
      // TODO Auto-generated method stub
      viewMedia(mediaRecorder);
      prepareMediaRecorder(mediaRecorder);
      //viewMedia(mediaRecorderD);
      //prepareMediaRecorder(mediaRecorderD);

    }
    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
      // TODO Auto-generated method stub

    }

    private void initMediaRecorder(MediaRecorder m, String path){

      CamcorderProfile profile = null;
//      if(CamcorderProfile.hasProfile(CamcorderProfile.QUALITY_720P)){
//        profile = CamcorderProfile.get(CamcorderProfile.QUALITY_720P);
//      }else if(CamcorderProfile.hasProfile(CamcorderProfile.QUALITY_480P)){
//
        profile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
//      }else{
//        profile = CamcorderProfile.get(CamcorderProfile.QUALITY_LOW);
//      }

      //profile = CamcorderProfile.get(CamcorderProfile.QUALITY_LOW);

      //m.setAudioSource(MediaRecorder.AudioSource.MIC);		
      m.setVideoSource(MediaRecorder.VideoSource.CAMERA);
      m.setOutputFormat(8);
      //m.setAudioEncoder(3);
      m.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
      m.setVideoFrameRate(profile.videoFrameRate);
      m.setVideoSize(profile.videoFrameWidth , profile.videoFrameHeight);
      m.setVideoEncodingBitRate(profile.videoBitRate);
      //m.setAudioChannels(1);
      //m.setAudioSamplingRate(30000);
      //m.setAudioEncodingBitRate(50000);

      LocalSocket sender = new LocalSocket();
      try {
        sender.connect(new LocalSocketAddress(SocketListener.SOCKET_ADDRESS));
        sender.setSendBufferSize(SocketListener.RECEIVER_BUFFER_SIZE);
        m.setOutputFile(sender.getFileDescriptor());
      } catch (IOException e1) {
        Log.e(TAG, "init socket error", e1);
      }
    }

    private void viewMedia(MediaRecorder m){
      m.setPreviewDisplay(surfaceHolder.getSurface());
    }


    private void prepareMediaRecorder(MediaRecorder m){
      try {
        m.prepare();
      } catch (IllegalStateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
}