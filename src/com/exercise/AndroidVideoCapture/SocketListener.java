package com.exercise.AndroidVideoCapture;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import com.exercise.AndroidVideoCapture.FileBufferManager.FileBufferUser;

import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.util.Log;


public class SocketListener extends Thread implements FileBufferUser{

  private final int HEADER_SIZE = 188 * 3;

  public final static String TAG = "SocketListener";
  public final static String SOCKET_ADDRESS = "my.socket";
  public final static int RECEIVER_BUFFER_SIZE = 262144;
  private RandomAccessFile mCurrentRandomAccessFile;
  private RandomAccessFile mNewRandomAccessFile;
  private FileBufferManager mFileBufferManager;
  private boolean mHeaderWritted;


  public SocketListener(){
    mHeaderWritted = false;
    mNewRandomAccessFile = null;
  }


  @Override
  public void run(){
    while(true){
      try {

        LocalServerSocket lServer = new LocalServerSocket(SOCKET_ADDRESS);
        while (true) {
          LocalSocket lReceiver = lServer.accept();
          lReceiver.setReceiveBufferSize(RECEIVER_BUFFER_SIZE);
          if (lReceiver != null) {
            InputStream lInput = lReceiver.getInputStream();
            Log.d(TAG, lInput.toString());
            byte[] lBuffer = new byte[RECEIVER_BUFFER_SIZE];

            while(true){

              int lNbByteRead = lInput.read(lBuffer);

              //TO-DO optimize
              if(!mHeaderWritted){
                lBuffer = extractHeaderAndSendToFileBufferManager(lBuffer, lNbByteRead);
                lNbByteRead = lBuffer.length;
              }
                         
              if(mCurrentRandomAccessFile != null){
                
                //if we have a new buffer we search the next frame i
                //first record of a file will be a frame i
                if(mNewRandomAccessFile != null){
                  int lOffsetIFrame = findIFrame(lBuffer, lNbByteRead);
                  if(lOffsetIFrame > -1){
                    //iframe detected
                    Log.d(TAG, "iframe detected");
                    if(lOffsetIFrame == 0){
                      //if 0 only write in new file
                      //switch file
                      mCurrentRandomAccessFile.close();
                      mCurrentRandomAccessFile = mNewRandomAccessFile;
                      mNewRandomAccessFile = null;
                      //write in new file
                      mCurrentRandomAccessFile.write(lBuffer, 0, lNbByteRead);
                    }
                    else{
                      byte[] lOldFileBuffer = new byte[lOffsetIFrame];
                      byte[] lNewFileBuffer = new byte[lNbByteRead - lOffsetIFrame];

                      splitBuffer(lBuffer, lNbByteRead, lOldFileBuffer, lNewFileBuffer, lOffsetIFrame);
                      //write in old file
                      mCurrentRandomAccessFile.write(lOldFileBuffer);

                      //switch file
                      mCurrentRandomAccessFile.close();
                      mCurrentRandomAccessFile = mNewRandomAccessFile;
                      mNewRandomAccessFile = null;

                      //write in new file
                      mCurrentRandomAccessFile.write(lNewFileBuffer);
                    }
                  }
                  else{
                    mCurrentRandomAccessFile.write(lBuffer, 0, lNbByteRead);
                  }
                }
                else{              
                  mCurrentRandomAccessFile.write(lBuffer, 0, lNbByteRead);
                }
              }
            }
          }
        }
      } catch (IOException e) {
        Log.e(TAG, "Run",e);
      }
    }
  }


  @Override
  public void onRandomAccessFileChanged(RandomAccessFile pRandomAccessFile)
  {
    if(mCurrentRandomAccessFile == null)
      mCurrentRandomAccessFile = pRandomAccessFile;
    else
      mNewRandomAccessFile = pRandomAccessFile;
  }

  public void setFileBufferManager(FileBufferManager pFileBufferManager){
    mFileBufferManager = pFileBufferManager;
  }

  private byte[] extractHeaderAndSendToFileBufferManager(byte[] pBytes, int pNbBytes){
    byte[] lReturn = new byte[pNbBytes - HEADER_SIZE];
    byte[] lHeader = new byte[HEADER_SIZE];

    splitBuffer(pBytes, pNbBytes, lHeader, lReturn, HEADER_SIZE); 

    if(mFileBufferManager != null)
      mHeaderWritted = mFileBufferManager.writeInHeader(lHeader);

    return lReturn;
  }

  public static void splitBuffer( byte[] pSrc, int pSrcSize, byte[] pSplitted1, byte[] pSplitted2, int pIndSplit ){
    if(pSrc != null && pSplitted1 != null && pSplitted2 != null && pSrcSize > 0 && pIndSplit < pSrcSize){
      System.arraycopy(pSrc, 0, pSplitted1, 0, pIndSplit);
      System.arraycopy(pSrc, pIndSplit, pSplitted2, 0, pSrcSize - pIndSplit);
    }
  }

  private int findIFrame(byte[] buffer, int read){
    int IframeOffsetMax=read/188;
    int returnState=-1;

    for (int IframeOffset=0;IframeOffset<IframeOffsetMax;IframeOffset++){
      int iframeOffset=IframeOffset*188;

      if(buffer[iframeOffset]==0x47){
        if (( (buffer[iframeOffset+5] & 0x38)>>3 )==1) {
          returnState= iframeOffset;
          break;
        }
      } 
    }
    return returnState;
  }
}
